#! /bin/bash

#post openroadm ROADMs
echo "- POSTING openroadm ROADMs..."
onos-netcfg localhost ./rigoletto_roadm_devices.json
sleep 4

#post openconfig transponders
echo "- POSTING openconfig transponders..."
onos-netcfg localhost ./rigoletto_transponder_devices.json
sleep 4

#post etsi qkd nodes
#onos-netcfg localhost ./rigoletto_qkd_devices.json
#sleep 5

#post links 
echo "- POSTING fiber links..."
onos-netcfg localhost ./rigoletto_links.json
sleep 4

